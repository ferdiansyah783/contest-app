import { CreatePostDto } from './dto/create-post.dto';
import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Param,
  ParseUUIDPipe,
  Post,
  Request,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { JwtAuthGuard } from 'src/utils/jwt-auth.guard';
import { PostsService } from './posts.service';

@Controller('posts')
export class PostsController {
  constructor(private readonly postService: PostsService) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  async create(@Body() createPostDto: CreatePostDto, @Request() req) {
    const user = req.user;
    return this.postService.create(user, createPostDto);
  }

  @Get()
  async findAll() {
    const [data, count] = await this.postService.findAll();
    return {
      data,
      count,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get(':id')
  async findPost(@Param('id', ParseUUIDPipe) id: string) {
    return {
      data: await this.postService.findOne(id),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }
}
