import { User } from 'src/users/entities/user.entity';
import { Role } from '../users/entities/role.entity';
import { Repository } from 'typeorm';
import { Inject, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { UsersService } from 'src/users/users.service';
import { comparePassword } from 'src/utils/bcrypt';

@Injectable()
export class AuthService {
  constructor(
    @Inject(UsersService) private readonly userService: UsersService,
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    private jwtService: JwtService,
  ) {}

  async register(createUser: CreateUserDto) {
    return this.userService.create(createUser);
  }

  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.userService.findUserByUsername(username);

    if (user) {
      const matched = comparePassword(pass, user.password);
      if (matched) {
        return user;
      }
      return null;
    }
    return null;
  }

  async login(user: any) {
    const existUser = await this.userRepository.findOne({
      relations: {
        roles: true,
      },
      where: {
        id: user.id,
      },
    });

    const payload = {
      username: existUser.username,
      sub: existUser.id,
      role: existUser.roles,
    };

    return {
      accessToken: this.jwtService.sign(payload),
    };
  }
}
