import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateRoleDto } from './dto/create-role.dto';
import { Role } from '../users/entities/role.entity';

@Injectable()
export class AdminService {
  constructor(
    @InjectRepository(Role) private rolesRepository: Repository<Role>,
  ) {}

  async create(createRoleDto: CreateRoleDto) {
    const newRole = await this.rolesRepository.insert(createRoleDto);

    return this.rolesRepository.findOneOrFail({
      where: {
        id: newRole.identifiers[0].id,
      },
    });
  }

  async showRoles() {
    return this.rolesRepository.findAndCount();
  }
}
