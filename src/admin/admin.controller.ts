import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Inject,
  Post,
} from '@nestjs/common';
import { AdminService } from './admin.service';
import { CreateRoleDto } from './dto/create-role.dto';

@Controller('admin')
export class AdminController {
  constructor(
    @Inject(AdminService) private readonly adminService: AdminService,
  ) {}

  @Post()
  async create(@Body() createRoleDto: CreateRoleDto) {
    return {
      data: await this.adminService.create(createRoleDto),
      statusCode: HttpStatus.CREATED,
      message: 'success',
    };
  }

  @Get('roles')
  async showRoles() {
    const [data, count] = await this.adminService.showRoles();

    return {
      data,
      count,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }
}
