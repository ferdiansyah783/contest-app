import { Contestan } from './../../contestan/entities/contestan.entity';
import { Role } from 'src/users/entities/role.entity';
import { Event } from 'src/events/entities/event.entity';
import { Post } from 'src/posts/entities/post.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  CreateDateColumn,
  ManyToMany,
  JoinTable,
  OneToMany,
  OneToOne,
} from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ unique: true })
  username: string;

  @Column({ unique: true })
  email: string;

  @Column()
  password: string;

  @Column({ default: '' })
  photoProfil: string;

  @Column({ default: '' })
  backgroundProfil: string;

  @Column({
    type: 'text',
    default: '',
  })
  about: string;

  @Column({ default: true })
  isActive: boolean;

  @CreateDateColumn({
    type: 'timestamp',
    nullable: false,
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    nullable: false,
  })
  updatedAt: Date;

  @DeleteDateColumn({
    type: 'timestamp',
    nullable: true,
  })
  deletedAt: Date;

  @ManyToMany(
    () => {
      return Role;
    },
    (role) => {
      return role.users;
    },
    { onUpdate: 'CASCADE' },
  )
  @JoinTable()
  roles: Role[];

  @OneToMany(
    () => {
      return Post;
    },
    (post) => {
      return post.user;
    },
  )
  posts: Post[];

  @OneToMany(() => Event, (event) => event.user)
  events: Event[];

  @OneToOne(() => Contestan, (contestan) => contestan.user)
  contestan: Contestan;
}
