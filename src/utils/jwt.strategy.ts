import { Role } from '../users/entities/role.entity';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { InjectRepository } from '@nestjs/typeorm';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { User } from 'src/users/entities/user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @InjectRepository(Role) private readonly roleRepository: Repository<Role>,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: 'superScret',
    });
  }

  userData: any;

  async validate(payload: any) {
    const existUserData = await this.userRepository.findOne({
      relations: {
        roles: true,
      },
      where: {
        id: payload.sub,
      },
    });

    this.userData = existUserData;

    if (!existUserData) {
      throw new HttpException(
        {
          statusCode: HttpStatus.UNAUTHORIZED,
          message: 'UNAUTHORIZED',
          data: 'Token is Invalid',
        },
        HttpStatus.UNAUTHORIZED,
      );
    }

    return {
      id: existUserData.id,
      username: existUserData.username,
      role: existUserData.roles,
    };
  }

  // authorizeRole(access) {
  //   if (this.userData.roles['id'] !== access) {
  //     throw new HttpException(
  //       {
  //         statusCode: HttpStatus.UNAUTHORIZED,
  //         message: 'UNAUTHORIZED',
  //         data: 'You dont have permission',
  //       },
  //       HttpStatus.UNAUTHORIZED,
  //     );
  //   }
  // }  
}
