import { Role } from 'src/users/entities/role.entity';
import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class ContestorRolesGuard implements CanActivate {
  constructor(
    @InjectRepository(Role) private roleRepository: Repository<Role>,
  ) {}

  async canActivate(context: ExecutionContext) {
    const requests = context.switchToHttp().getRequest();
    const user = requests.user.role[0].id;
    const role = await this.roleRepository.findOne({
      where: {
        id: 2,
      },
    });

    if (user !== role.id) {
      throw new UnauthorizedException('You are not a Contestor');
    }

    return true;
  }
}
