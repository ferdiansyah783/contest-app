import { Module } from '@nestjs/common';
import { ContestanService } from './contestan.service';

@Module({
  providers: [ContestanService]
})
export class ContestanModule {}
