import { Test, TestingModule } from '@nestjs/testing';
import { ContestanService } from './contestan.service';

describe('ContestanService', () => {
  let service: ContestanService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ContestanService],
    }).compile();

    service = module.get<ContestanService>(ContestanService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
