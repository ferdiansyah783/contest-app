import {
  IsDate,
  IsDecimal,
  IsNotEmpty,
  IsString,
  IsUUID,
  MinDate,
  MinLength,
} from 'class-validator';
import { User } from 'src/users/entities/user.entity';

export class CreateEventDto {
  @IsNotEmpty()
  @IsString()
  @MinLength(3)
  title: string;

  @IsNotEmpty()
  @IsDecimal()
  competitionPrice: string;

  @IsNotEmpty()
  thumbnail: string;

  @IsNotEmpty()
  @IsString()
  description: string;

  @IsNotEmpty()
  deadline: Date;

  user: User;
}
