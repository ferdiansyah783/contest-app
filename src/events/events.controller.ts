import { ContestorRolesGuard } from '../utils/role/contestor-roles.guard';
import { JwtAuthGuard } from './../utils/jwt-auth.guard';
import { CreateEventDto } from './dto/create-event.dto';
import {
  Body,
  Controller,
  Post,
  Request,
  HttpStatus,
  UseGuards,
  Get,
  Param,
  ParseUUIDPipe,
} from '@nestjs/common';
import { EventsService } from './events.service';

@Controller('events')
export class EventsController {
  constructor(private readonly eventService: EventsService) {}

  @UseGuards(JwtAuthGuard, ContestorRolesGuard)
  @Post()
  async create(@Body() createEventDto: CreateEventDto, @Request() req) {
    const user = req.user;
    return {
      data: await this.eventService.create(user, createEventDto),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get()
  async findAll() {
    const [data, count] = await this.eventService.findAll();

    return {
      data,
      count,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('user/:id')
  async findEvent(@Param('id', ParseUUIDPipe) id: string) {
    return {
      data: await this.eventService.findOne(id),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get(':id')
  async findEventById(@Param('id', ParseUUIDPipe) id: string) {
    return {
      data: await this.eventService.findOneById(id),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }
}
