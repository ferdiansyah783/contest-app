import { UpdateEventDto } from './dto/update-event.dto';
import { User } from 'src/users/entities/user.entity';
import { CreateEventDto } from './dto/create-event.dto';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EntityNotFoundError, Repository } from 'typeorm';
import { Event } from './entities/event.entity';

@Injectable()
export class EventsService {
  constructor(
    @InjectRepository(Event)
    private readonly eventRepository: Repository<Event>,
  ) {}

  async create(user: User, createEventDto: CreateEventDto) {
    const event = new Event();

    event.title = createEventDto.title;
    event.competitionPrice = createEventDto.competitionPrice;
    event.thumbnail = createEventDto.thumbnail;
    event.deadline = createEventDto.deadline;
    event.description = createEventDto.description;
    event.user = user;

    return await this.eventRepository.save(event);
  }

  findAll() {
    return this.eventRepository.findAndCount({ relations: { user: true } });
  }

  async findOne(id: string) {
    try {
      return await this.eventRepository.find({
        relations: {
          user: true,
        },
        where: {
          user: { id },
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async findOneById(id: string) {
    try {
      return await this.eventRepository.findOneOrFail({
        relations: {
          user: true,
        },
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async update(id: string, updateEventDto: UpdateEventDto) {
    try {
      await this.eventRepository.findOneOrFail({
        relations: {
          user: true,
        },
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }

    await this.eventRepository.update(id, updateEventDto);

    return this.eventRepository.findOneOrFail({
      where: {
        id,
      },
    });
  }

  async remove(id: string) {
    try {
      await this.eventRepository.findOneOrFail({
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }

    await this.eventRepository.delete(id);
  }
}
